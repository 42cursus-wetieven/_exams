#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef struct client_s {
	int fd;
	int id;
	_Bool newMsg;
}	client_t;

client_t clients[1024];
fd_set readFds, writeFds, allFds;
int servfd, nxt_id = 0;
char msg[64];

void bye()
{
	write(2, "Fatal error\n", 12);
	close(servfd);
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd > 0)
			close(clients[i].fd);
	}
	exit(1);
}

void broadcastMsg(int senderFd, char *msg, int msgLen)
{
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd > 0
			&& clients[i].fd != senderFd
			&& FD_ISSET(clients[i].fd, &writeFds))
		{
			if (send(clients[i].fd, msg, msgLen, 0) < 0)
				bye();
		}
		if (clients[i].fd == 0)
			break;
	}
}

client_t *fdToClient(int fd)
{
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd == fd)
			return (&clients[i]);
		if (clients[i].fd == 0)
			break;
	}
	return NULL;
}

int main(int ac, char **av) {
	int connfd;
	socklen_t addrlen;
	struct sockaddr_in servaddr, cliaddr; 

	if (ac != 2)
	{
		write(2, "Wrong number of arguments\n", 26);
		exit(1);
	}
	uint16_t port = atoi(av[1]);

// #### SERVER CREATION #######################################################

	// socket create and verification 
	servfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (servfd == -1) { 
		bye();
	} 
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(2130706433); //127.0.0.1
	servaddr.sin_port = htons(port); 
  
	// Binding newly created socket to given IP and verification 
	if ((bind(servfd, (const struct sockaddr *)&servaddr, sizeof(servaddr))) != 0) { 
		bye();
	} 
	if (listen(servfd, 10) != 0) {
		bye();
	}
	FD_ZERO(&allFds);
	FD_SET(servfd, &allFds);

// ####################################################### SERVER CREATION ####

// #### CLIENT POLLING LOOP ###################################################

	while (1)
	{
		writeFds = readFds = allFds; // Reset fd_sets

		if (select(1025, &readFds, &writeFds, NULL, NULL) < 0) // Update fd_sets
			continue;

		for (int fd = 0; fd <= 1024; ++fd)
		{
			if (FD_ISSET(fd, &readFds)) // Find readable fds
			{
				if (fd == servfd)
				{ // (the server's fd is readable when a new client wants to connect)
					// CONNECT NEW CLIENT
					addrlen = sizeof(cliaddr);
					connfd = accept(servfd, (struct sockaddr *)&cliaddr, &addrlen);
					if (connfd < 0) { 
						bye();
					} 
					FD_SET(connfd, &allFds);

					// Add client to list
					for (int i = 0; i < 1024; ++i)
					{
						if (clients[i].fd <= 0)
						{
							clients[i].id = nxt_id;
							clients[i].fd = connfd;
							clients[i].newMsg = 1;
							break;
						}
					}

					// Announce new client
					bzero(msg, strlen(msg));
					sprintf(msg, "server: client %d just arrived\n", nxt_id++);
					broadcastMsg(connfd, msg, strlen(msg));
					// Restart the polling loop
					break;
				}

				// Identify client attached to current fd
				client_t *client = fdToClient(fd);

				// Read that fd
				char c;
				int rec_status = recv(fd, &c, 1, 0);

				if (rec_status <= 0) // if no client is connected to that fd
				{
					// Announce client has left and remove if from client list
					bzero(msg, strlen(msg));
					sprintf(msg, "server: client %d just left\n", client->id);
					broadcastMsg(fd, msg, strlen(msg));
					// Remove client's fd from fd_set
					FD_CLR(fd, &allFds);
					// Close its fd
					close(fd);
					// Remove client from client array
					client->id = -1;
					client->fd = -1;
					// Restart polling loop
					break;
				}

				if (client->newMsg)
				{ // ANNOUNCE NEW MESSAGE
					bzero(msg, strlen(msg));
					sprintf(msg, "client %d: ", client->id);
					broadcastMsg(fd, msg, strlen(msg));
				}

				broadcastMsg(fd, &c, 1); // SEND MESSAGE

				client->newMsg = (c == '\n'); // FINISH MESSAGE, reset client flag
			}
		}
	}
// ################################################### CLIENT POLLING LOOP ####

	return (0);
}
