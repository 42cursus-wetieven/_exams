#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef struct client_s {
	int fd;
	int id;
	_Bool newMsg;
}	client_t;

client_t clients[1024];
fd_set writeFds, readFds, allFds;
int servfd, nxtId = 0;
char msg[64];

void bye()
{
	write(2, "Fatal error\n", 12);
	close(servfd);
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd > 0)
			close(clients[i].fd);
		if (clients[i].fd == 0)
			break;
	}
	exit(1);
}

void broadcastMsg(int senderFd, char *msg, int msgLen)
{
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd > 0
			&& clients[i].fd != senderFd
			&& FD_ISSET(clients[i].fd, &writeFds))
		{
			if (send(clients[i].fd, msg, msgLen, 0) < 0)
				bye();
		}
		if (clients[i].fd == 0)
			break;
	}
}

client_t *fdToClient(int fd)
{
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd == fd)
			return (&clients[i]);
		if (clients[i].fd == 0)
			break;
	}
	return NULL;
}

int main(int ac, char **av) {

	if (ac != 2)
	{
		write(2, "Wrong number of arguments\n", 26);
		exit(1);
	}

	int connfd;
	socklen_t len;
	struct sockaddr_in servaddr, cli; 
	uint16_t port = atoi(av[1]);

	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(2130706433); //127.0.0.1
	servaddr.sin_port = htons(port); 

	// socket create and verification 
	servfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (servfd == -1) { 
		bye();
	} 
  
	// Binding newly created socket to given IP and verification 
	if ((bind(servfd, (const struct sockaddr *)&servaddr, sizeof(servaddr))) != 0) { 
		bye();
	} 
	if (listen(servfd, 10) != 0) {
		bye();
	}
	FD_ZERO(&allFds);
	FD_SET(servfd, &allFds);

	while (1)
	{
		writeFds = readFds = allFds;

		if (select(1025, &readFds, &writeFds, NULL, NULL) < 0)
			continue;

		for (int fd = 0; fd <= 1024; ++fd)
		{
			if (FD_ISSET(fd, &readFds))
			{
				if (fd == servfd)
				{
					len = sizeof(cli);
					connfd = accept(servfd, (struct sockaddr *)&cli, &len);
					if (connfd < 0) { 
						bye();
					} 
					FD_SET(connfd, &allFds);
					for (int i = 0; i < 1024; ++i)
					{
						if (clients[i].fd <= 0)
						{
							clients[i].fd = connfd;
							clients[i].id = nxtId;
							clients[i].newMsg = 1;
							break;
						}
					}
					bzero(msg, strlen(msg));
					sprintf(msg, "server: client %d just arrived\n", nxtId++);
					broadcastMsg(connfd, msg, strlen(msg));
					break;
				}

				client_t *client = fdToClient(fd);

				char c;
				int recStatus = recv(fd, &c, 1, 0);

				if (recStatus <= 0)
				{
					bzero(msg, strlen(msg));
					sprintf(msg, "server: client %d just left\n", client->id);
					broadcastMsg(fd, msg, strlen(msg));
					FD_CLR(fd, &allFds);
					close(fd);
					client->fd = -1;
					client->id = -1;
					break;
				}

				if (client->newMsg)
				{
					bzero(msg, strlen(msg));
					sprintf(msg, "client %d: ", client->id);
					broadcastMsg(fd, msg, strlen(msg));
				}
				broadcastMsg(fd, &c, 1);

				client->newMsg = (c == '\n');
			}
		}
	}
}
