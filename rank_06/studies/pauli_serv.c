#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef struct s_client {
	int id;
	int fd;
} t_client;

//Globals
int serv_fd, nxt_id;
fd_set readableFds, writableFds, allFds;
char buf[1024], msg[2048];
t_client clients[1024];

void fatal(){
	write(2, "Fatal error\n", 12);
	close(serv_fd);
	exit(1);
}

int	get_id(int fd)
{
	for (int i = 0; i <= 1024; i++)
	{
		if (clients[i].fd == fd)
			return (clients[i].id);
	}
	return -1;
}

void	send_all(int fd){
	for (int i = 0; i <= 1024; i++)
	{
		if (clients[i].fd != fd && clients[i].fd != 0)
		{
			if (FD_ISSET(clients[i].fd, &writableFds))
				send(clients[i].fd, &msg, strlen(msg), 0);
		}
	}
	bzero(msg, strlen(msg));	
}

void add_cli(int fd){
	int n_id = nxt_id;

		
	for (int i = 0; i <= 1024; i++)
	{
		if(clients[i].fd == 0 && clients[i].id == 0)
		{
			clients[i].fd = fd;
			clients[i].id = n_id;
			break;
		}
	}
	FD_SET(fd, &allFds);
	sprintf(msg, "server: client %d just arrived\n", n_id);
	send_all(fd);
	nxt_id++;
}

void rm_cli(int fd){
	for (int i = 0; i <= 1024; i++)
	{
		if(clients[i].fd == fd)
		{
			FD_CLR(fd, &allFds);
			close(fd);
			sprintf(msg, "server: client %d just left\n", clients[i].id);
			
			clients[i].fd = 0;
			clients[i].id = 0;
			send_all(fd);
			return;
		}

	}
}

void extract_msg(int fd){
	sprintf(msg, "client %d: %s", get_id(fd), buf);
	send_all(fd);
	bzero(buf, strlen(buf));
}

int main(int ac, char **av) {

	if (ac != 2)
	{
		write(2, "wrong nb\n", 9);
		exit(1);
	}
	struct sockaddr_in servaddr, cli; 
	
	bzero(clients, 1024);
	bzero(msg, 2048);
	bzero(buf, 1024);
	// socket create and verification 
	serv_fd = socket(AF_INET, SOCK_STREAM, 0); 
	if (serv_fd == -1) { 
		fatal(); 
	} 
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(2130706433); //127.0.0.1
	servaddr.sin_port = htons(atoi(av[1])); 
  
	// Binding newly created socket to given IP and verification 
	if ((bind(serv_fd, (const struct sockaddr *)&servaddr, sizeof(servaddr))) != 0) { 
		fatal(); 
	} 
	if (listen(serv_fd, 10) != 0) {
		fatal(); 
	}
	FD_ZERO(&allFds);
	FD_SET(serv_fd, &allFds);

	int new_cli_fd;
	for (;;)
	{
		writableFds = readableFds = allFds;
		if (select(1025, &readableFds, &writableFds, NULL, NULL) < 0)
			continue;
		for (int fd = 0; fd < 1024; ++fd)
		{
			if (FD_ISSET(fd, &readableFds))
			{
				if (fd == serv_fd) {
					socklen_t len = sizeof(cli);

					new_cli_fd = accept(serv_fd, (struct sockaddr *)&cli, &len);
					if (new_cli_fd < 0) { 
						fatal(); 
					}
					add_cli(new_cli_fd);
					break;
				}
				bzero(msg, 2048);
				bzero(buf, 1024);
				int ret_recv = 1;

				while (ret_recv == 1 && buf[strlen(buf) - 1] != '\n')
				{
					ret_recv = recv(fd, buf + strlen(buf), 1, 0);
				}
				if (ret_recv <= 0)
				{
					rm_cli(fd);
					bzero(buf, strlen(buf));
					break;
				}
				extract_msg(fd);
			}
		}
	}
}
