// 4 GENERALIST LIBRARIES
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h> // String manipulation lib

// 4 NETWORKING LIBS
#include <netdb.h> // Networking database operations
#include <netinet/in.h> // Internet addresses
// Sockets and select() (socket polling)
#include <sys/socket.h>
#include <sys/select.h> // may not be necessary

// Optional aliases to sockaddr types save on typing/bloat later on
typedef struct sockaddr addr_t; // generic addr struct, used once for bind()
typedef struct sockaddr_in addr_in_t; // IPv4 addr struct

// Client struct to manage the server connections as the subject's asks
typedef struct {
	int id;
	int fd;
	_Bool newMsg;
}	client_t;

// GLOBAL VARIABLES
client_t clients[1024]; // client queue, select() is ancient and can't poll more than 1024 fds
int serv_fd, next_id = 0; // next_id holds the next available client id
fd_set allFds, readableFds, writableFds; // a fd_set holds the fds to poll
char msg[42]; // Will hold our messages on the stack (avoiding mem alloc)

void bye()
{ // Muy fatal
	write(2, "Fatal error\n", 12); // Message on stderr
	close(serv_fd); // Close server fd
	for (int i = 0; i < 1024; ++i)
	{
		if (clients[i].fd > 0)
			close(clients[i].fd); // Close clients fds
	}
	exit(1);
}

void create_server(uint16_t port)
{
	addr_in_t addr; // Let's create and initialize our server's address struct
	bzero(&addr, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(0x7F000001); // 127.0.0.1
	addr.sin_port = htons(port); // Server's port chosed via arguments

	if ((serv_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) // Create socket (fd)
		bye();
	if (bind(serv_fd, (addr_t *)&addr, sizeof(addr)) < 0) // Give it an address
		bye();
	if (listen(serv_fd, 0) < 0) // Start reception on that socket
		bye();

	FD_ZERO(&allFds); // Initializing the set holding the server's connections fds
	FD_SET(serv_fd, &allFds); // Adding the server's fd to it
}

// REJOICE ! max_fd() CAN APPARENTLY BE REPLACED BY THE VALUE "1024" !
/* int max_fd() */
/* { // Since fd's are alloted incrementally... */
/* 	int max = serv_fd; //...starting at our server's fd... */
/* 	for (int i = 0; i < 1024; ++i) { //...we iterate through our clients list... */
/* 		if (clients[i].fd > max) { */
/* 			max = clients[i].fd; //...to find which client has the highest fd. */
/* 		} */
/* 		if (clients[i].fd == 0) */
/* 			break ; */
/* 	} */
/* 	return max; */
/* } */

client_t *get_client(int fd)
{
	for (int i = 0; i < 1024; ++i) {
		if (clients[i].fd == fd) {
			return &clients[i];
		}
		if (clients[i].fd == 0)
			break ;
	}
	return NULL; // This seems like a loose end xD
}

void send_to_all(int sender_fd, char *msg, int len)
{
	for (int i = 0; i < 1024; ++i) {
		if (clients[i].fd > 0 // fd is valid
			&& clients[i].fd != sender_fd // fd isn't sender's
			&& FD_ISSET(clients[i].fd, &writableFds)) { // fd is writable
			if (send(clients[i].fd, msg, len, 0) < 0)
				bye();
		}
		if (clients[i].fd == 0) // unitialized fd == end of client list ?
			break ;
	}
}

int add_client_to_list(int fd)
{
	for (int i = 0; i < 1024; ++i) {
		if (clients[i].fd <= 0) {
			clients[i].fd = fd;
			clients[i].id = next_id++;
			clients[i].newMsg = 1;
			return clients[i].id;
		}
	}
	return -1;
}

// Creates a client socket on our server
void add_client()
{
	addr_in_t clientaddr;
	socklen_t len = sizeof(clientaddr); // is it a winter sock or a summer sock?
	int new_sock_fd;

	if ((new_sock_fd = accept(serv_fd, (addr_t *)&clientaddr, &len)) < 0)
		bye(); // Termination upon failed socket creation on our server
	sprintf(msg, "server: client %d just arrived\n", add_client_to_list(new_sock_fd));
	send_to_all(new_sock_fd, msg, strlen(msg));
	FD_SET(new_sock_fd, &allFds);
}

int rm_client(client_t *client)
{
	int id = client->id;

	client->fd = -1;
	client->id = -1;
	return id;
}

int main(int ac, char **av)
{
	if (ac != 2) {
		write(2, "Wrong number of arguments\n", 26);
		return 1;
	}

	uint16_t port = atoi(av[1]); // this uint16_t typing is necessary to pass test 4
	create_server(port);

	for (;;) {
		writableFds = readableFds = allFds;
		// Select takes the fds range and rw fd_sets to check if they're open
		if (select(1024 + 1, &readableFds, &writableFds, NULL, NULL) < 0)
			continue ;
		for (int fd = 0; fd <= 1024; ++fd) // Cycling through our clients fds
		{
			if (FD_ISSET(fd, &readableFds))
			{ // We can read from this fd
				if (fd == serv_fd)
				{ // The fd is our server's, which means a new client wants to connect
					bzero(&msg, sizeof(msg));
					add_client();
					break ;
				}

				// Or we just try to get the client alloted to that fd
				client_t *client = get_client(fd);

				// And we try to get its message
				char c; // which we do one char at a time to keep code terse
				int ret_recv = recv(fd, &c, 1, 0);

				if (ret_recv <= 0)
				{ // if client has left we signal it to others and close its fd.
					bzero(&msg, sizeof(msg));
					sprintf(msg, "server: client %d just left\n", rm_client(client));
					send_to_all(fd, msg, strlen(msg));
					FD_CLR(fd, &allFds);
					close(fd);
					break ;
				}

				if (client->newMsg)
				{ // If our client has a new message we signal it to everyone
					bzero(&msg, sizeof(msg));
					sprintf(msg, "client %d: ", client->id);
					send_to_all(fd, msg, strlen(msg));
				}
				// Then we send it character by character, by redoing the for() loop
				send_to_all(fd, &c, 1);

				// Encountering a newline means our message is complete,
				// so we know that this fd won't be readable anymore,
				// thus we reset our client for its eventual next message.
				client->newMsg = (c == '\n');
			}
		}
	}
}
