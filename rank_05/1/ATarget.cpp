#include "ATarget.hpp"

// Destructor
//
ATarget::~ATarget()
{

}

// Constructors
//
ATarget::ATarget()
{

}

ATarget::ATarget(std::string const& type)
:
	type(type)
{

}

// Copy construction and assignment
//
ATarget::ATarget(ATarget const& source)
{
	*this = source;
}

ATarget & ATarget::operator=(ATarget const& source)
{
	this->type = source.type;
	return (*this);
}

// Accessors
//
std::string const& ATarget::getType() const
{
	return (this->type);
}

// Functions
//
void ATarget::getHitBySpell(ASpell const& spell) const
{
	std::cout << this->type << " has been " << spell.getEffects() << "!" <<
	std::endl;
}
