#include "ASpell.hpp"

// Destructor
//
ASpell::~ASpell()
{

}

// Constructors
//
ASpell::ASpell()
{

}

ASpell::ASpell(std::string const& name, std::string const& effects)
:
	name(name), effects(effects)
{

}

// Copy construction and assignment
//
ASpell::ASpell(ASpell const& source)
{
	*this = source;
}

ASpell & ASpell::operator=(ASpell const& source)
{
	this->name = source.name;
	this->effects = source.effects;
	return (*this);
}

// Accessors
//
std::string const& ASpell::getName() const
{
	return (this->name);
}

std::string const& ASpell::getEffects() const
{
	return (this->effects);
}

// Functions
//
void ASpell::launch(ATarget const& target) const
{
	target.getHitBySpell(*this);
}
