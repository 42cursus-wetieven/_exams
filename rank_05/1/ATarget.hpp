#pragma once

#include <iostream>

class ASpell;

class ATarget
{
	protected:
		std::string type;

	public:
		virtual ~ATarget();

		ATarget();
		ATarget(std::string const& type);

		ATarget(ATarget const& source);
		ATarget& operator=(ATarget const& source);

		std::string const& getType() const;

		virtual ATarget* clone() const = 0;

		void getHitBySpell(ASpell const& spell) const;
};

#include "ASpell.hpp"
