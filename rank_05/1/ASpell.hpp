#pragma once

#include <iostream>

class ATarget;

class ASpell
{
	protected:
		std::string name;
		std::string effects;

	public:
		virtual ~ASpell();

		ASpell();
		ASpell(std::string const& name, std::string const& effects);

		ASpell(ASpell const& source);
		ASpell& operator=(ASpell const& source);

		std::string const& getName() const;
		std::string const& getEffects() const;

		virtual ASpell* clone() const = 0;

		void launch(ATarget const& target) const;
};

#include "ATarget.hpp"
