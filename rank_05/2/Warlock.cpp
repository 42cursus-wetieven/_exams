#include "Warlock.hpp"

// Default destructor
//
Warlock::~Warlock()
{
	std::cout << this->name << ": My job here is done!" << std::endl;
}

// Parametric constructor
//
Warlock::Warlock(std::string const& name, std::string const& title)
:
	name(name), title(title)
{
	std::cout << this->name << ": This looks like another boring day." << std::endl;
}

// Getters
//
std::string const& Warlock::getName() const
{
	return (this->name);
}

std::string const& Warlock::getTitle() const
{
	return (this->title);
}

// Setters
//
void Warlock::setTitle(std::string const& title)
{
	this->title = title;
}

// Functions
//
void Warlock::introduce() const
{
	std::cout << this->name << ": I am " << this->name
	<< ", " << this->title << "!" << std::endl;
}

void Warlock::learnSpell(ASpell* spell)
{
	spellBook.learnSpell(spell);
}

void Warlock::forgetSpell(std::string const& spell)
{
	spellBook.forgetSpell(spell);
}

void Warlock::launchSpell(std::string const& spellName, ATarget const& target)
{
	ASpell* spell = spellBook.createSpell(spellName);
	if (spell)
		spell->launch(target);
}
