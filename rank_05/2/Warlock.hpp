#pragma once

#include <iostream>

#include "SpellBook.hpp"

class Warlock
{
	private:
		std::string name;
		std::string title;

		Warlock();
		Warlock(Warlock const& source);
		Warlock& operator=(Warlock const& source);

		SpellBook spellBook;

	public:
		~Warlock();

		Warlock(std::string const& name, std::string const& title);

		std::string const& getName() const;
		std::string const& getTitle() const;

		void setTitle(std::string const& title);

		void introduce() const;

		void learnSpell(ASpell *spell);
		void forgetSpell(std::string const& spellName);
		void launchSpell(std::string const& spellName, ATarget const& target);
};
