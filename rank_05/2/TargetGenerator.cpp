#include "TargetGenerator.hpp"

TargetGenerator::~TargetGenerator()
{
	// Free whatever memory the targetArray is using
	std::map<std::string, ATarget*>::iterator it = this->targetArray.begin();
	std::map<std::string, ATarget*>::iterator end = this->targetArray.end();
	while (it != end)
	{
		delete it->second;
		++it;
	}
	this->targetArray.clear();
}

TargetGenerator::TargetGenerator()
{
}

void TargetGenerator::learnTargetType(ATarget* target)
{
	if (target)
		targetArray.insert
		(std::pair<std::string, ATarget*>(target->getType(), target->clone()));
}

void TargetGenerator::forgetTargetType(std::string const& targetName)
{
	std::map<std::string, ATarget*>::iterator it = targetArray.find(targetName);
	if (it != targetArray.end())
		delete it->second;
	targetArray.erase(targetName);
}

ATarget* TargetGenerator::createTarget(std::string const& targetName)
{
	std::map<std::string, ATarget*>::iterator it = targetArray.find(targetName);
	if (it != targetArray.end())
		return (targetArray[targetName]);
	return NULL;
}
