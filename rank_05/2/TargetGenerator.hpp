#pragma once

#include "ATarget.hpp"

#include <map>

class TargetGenerator
{
	private:
		std::map<std::string, ATarget*> targetArray;

		TargetGenerator(TargetGenerator const& source);
		TargetGenerator& operator=(TargetGenerator const& source);

	public:
		~TargetGenerator();

		TargetGenerator();

		void learnTargetType(ATarget *target);
		void forgetTargetType(std::string const& targetName);
		ATarget* createTarget(std::string const& targetName);
};
