#pragma once

#include <map>

#include "ASpell.hpp"

class SpellBook
{
	private:
		std::map<std::string, ASpell*> spellArray;

		SpellBook(SpellBook const& source);
		SpellBook& operator=(SpellBook const& source);

	public:
		~SpellBook();

		SpellBook();

		void learnSpell(ASpell* spell);
		void forgetSpell(std::string const& spellName);
		ASpell* createSpell(std::string const& spellName);
};
