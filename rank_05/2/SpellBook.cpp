#include "SpellBook.hpp"

SpellBook::~SpellBook()
{
	// Free whatever memory the spellArray is using
	std::map<std::string, ASpell*>::iterator it = this->spellArray.begin();
	std::map<std::string, ASpell*>::iterator end = this->spellArray.end();
	while (it != end)
	{
		delete it->second;
		++it;
	}
	this->spellArray.clear();
}

SpellBook::SpellBook()
{
}

void SpellBook::learnSpell(ASpell* spell)
{
	if (spell)
		spellArray.insert
		(std::pair<std::string, ASpell*>(spell->getName(), spell->clone()));
}

void SpellBook::forgetSpell(std::string const& spellName)
{
	std::map<std::string, ASpell*>::iterator it = spellArray.find(spellName);
	if (it != spellArray.end())
		delete it->second;
	spellArray.erase(spellName);
}

ASpell* SpellBook::createSpell(std::string const& spellName)
{
	/* std::map<std::string, ASpell*>::iterator it = spellArray.find(spellName); */
	/* if (it != spellArray.end()) */
	/* 	return (spellArray[spellName]); */
	/* return NULL; */

	std::map<std::string, ASpell*>::iterator it = spellArray.find(spellName);
	if (it != spellArray.end())
		return it->second;
	return NULL;

	/* return spellArray[spellName]; */
}
