#pragma once

#include <iostream>

class Warlock
{
	private:
		std::string name;
		std::string title;

		Warlock();
		Warlock(Warlock const& source);
		Warlock& operator=(Warlock const& source);


	public:
		~Warlock();

		Warlock(std::string const& name, std::string const& title);

		std::string const& getName() const;
		std::string const& getTitle() const;

		void setTitle(std::string const& title);

		void introduce() const;
};
