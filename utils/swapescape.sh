# This command allows to swap escape and caps, to switch modes in vim faster
setxkbmap -option caps:swapescape
