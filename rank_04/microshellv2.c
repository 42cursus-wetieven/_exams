#include <unistd.h>
#include <stdlib.h>
#include <string.h>

size_t	ft_strlen(const char *str)
{
	const char	*ptr;

	ptr = str;
	while (*ptr)
		ptr++;
	return (ptr - str);
}

void    ft_puterr(char *s) { write(2, s, ft_strlen(s)); }

void	exec(char **av, char **env, int in, int out)
{
	int	pid;

	if (av[0]) // Seems clearer to make that test here instead of burying it in an "else if".
	{
		if (strcmp(av[0], "cd") == 0) // Also avoiding brainfarts by expliciting strcmp expected returns, and avoiding "double negative" assumptions on its return.
		{
			if (!av[1] || (av[1] && av[2]))
				ft_puterr("error: cd: bad arguments\n");
			else if (chdir(av[1]) == -1)
			{
				ft_puterr("error: cd: cannot change directory to ");
				ft_puterr(av[1]);
				ft_puterr("\n");
			}
		}
		else
		{
			pid = fork();
			if (pid == 0)
			{
				if (in != STDIN_FILENO)
				{
					dup2(in, STDIN_FILENO);
					close(in);
				}
				if (out != STDOUT_FILENO)
				{
					dup2(out, STDOUT_FILENO);
					close(out);
				}
				execve(av[0], av, env);
				ft_puterr("error: cannot execute ");
				ft_puterr(av[0]);
				ft_puterr("\n");
				exit(-1);
			}
		}
	}
}

int	main(int ac, char **av, char **env)
{
	int	start = 0, end = start, cmd_in = STDIN_FILENO;
	int	fd[2];

	(void)ac;
	av = &av[1];
	while (av[end])
	{
		if (strcmp(av[end], "|") == 0)
		{
			av[end] = NULL;
			pipe(fd);
			exec(av + start, env, cmd_in, fd[1]);
			close(fd[1]);
			if (cmd_in != STDIN_FILENO)
				close(cmd_in);
			cmd_in = fd[0];
			end++;
			start = end;
		}
		else if (strcmp(av[end], ";") == 0)
		{
			av[end] = NULL;
			exec(av + start, env, cmd_in, STDOUT_FILENO);
			while (waitpid(-1, NULL, 0) != -1)
				;
			if (cmd_in != STDIN_FILENO)
				close(cmd_in);
			cmd_in = STDIN_FILENO;
			end++;
			start = end;
		}
		else
			end++;
	}
	if (end > 0 && av[end - 1] != NULL)
		exec(av + start, env, cmd_in, STDOUT_FILENO);
	while (waitpid(-1, NULL, 0) != -1)
		;
	if (cmd_in != STDIN_FILENO) // Don't know why this was above waitpid in the original version, it seems to work here, but let's keep that in mind if unexpected bugs appear.
		close(cmd_in);
	return 0;
}
