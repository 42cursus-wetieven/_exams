## Parsing (main)

- Déclarer 4 variables de type int :
	- int : start, end, cmd_in_fd initialisées à "0".
	- int : fd[2] pour les pipe

- Evaluation des arguments et mise en forme
- Itérer sur av jusqu'à rencontrer un délimiteur de commande
- Passer a NULL les délimiteurs pour cadastrer les commandes passées à execve
- | (Créer un pipe si ce délimiteur est un pipe)
- Lancer l'exec en lui passant :
	- | (La sortie d'écriture du pipe (fd[1]) pour un pipe)
	- La sortie standard le reste du temps
- Puis selon le délimiteur rencontré :
	- | On ferme la sortie du pipe
	- Ou on attend la fin de l'execution de notre commande (waitpid)
- Rafraichir le fd in de la commande simple en cours de parsing. En le fermant s'il est différent de STDIN, puis :
	- | En le définissant sur celui du pipe que l'on vient d'ouvrir
	- En le remettant à "0"
- Si l'on vient de parser un délimiteur de commande complexe, on incrémente l'itérateur marquant le démarrage d'une commande simple à la valeur de l'itérateur marquant la fin de notre parsing.
- En sortie de boucle, il convient de tester si nous avons bien parsé des commandes et que la dernière commande n'est pas un délimiteur, le cas échéant on (re)lance le processus d'execution.
- Puis nous attendons la fin des processus enfants (while (waitpid(-1, NULL, 0) != -1);)...
- ... avant de retourner 0;

## Exec

```
void	exec(char ** av, char **env, int in, int out);
```
- int : pid;

- Vérifier si nous avons une chaîne non nulle en entrée...
- ...puis si cette chaîne est "cd".
	- Si la commande est cd et que ses arguments sont valides, on opère un chdir sans forker.
		- Si son argument est absent ou multiple nous affichons "error: cd: bad arguments\n".
		- Si chdir retourne -1, nous affichons "error: cd: cannot change directory to [path]\n"
- Dans le cas général on opère un fork
- Puis s'ils diffèrent des fds standard, nous écrasons les entrées et sorties standard par les entrées et sorties de la commande courante dans le processus enfant. Avant de fermer les fd d'entrée sortie originaux de la commande courante;
- Nous appelons ensuite execve sur la commande simple que nous avons parsée.
	- Si execve échoue, nous affichons "error: cannot execute [command]\n" puis nous effectuons un exit(-1);.
