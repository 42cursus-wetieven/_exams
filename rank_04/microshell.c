#include <unistd.h>
#include <stdlib.h>
#include <string.h>

size_t	ft_strlen(const char *str)
{
	const char	*ptr;

	ptr = str;
	while (*ptr)
		ptr++;
	return (ptr - str);
}

void    ft_putstr(char *s) { write(2, s, ft_strlen(s)); }

void    exec(char **av, char **env, int in, int out)
{
	int	pid;

	if (av[0] && strcmp(av[0], "cd")) { // if we DON'T match "cd"
		pid = fork(); // then we fork
		if (pid == 0) {
			if (in != STDIN_FILENO) {
				dup2(in, STDIN_FILENO);
				close(in);
			}
			if (out != STDOUT_FILENO) {
				dup2(out, STDOUT_FILENO);
				close(out);
			}
			execve(av[0], av, env);
			ft_putstr("error: cannot execute ");
			ft_putstr(av[0]);
			ft_putstr("\n");
			exit(-1);
		}
	} else if (av[0]) { // if we match "cd" then
		if (!av[1] || (av[1] && av[2]))
			ft_putstr("error: cd: bad arguments\n");
		else if (chdir(av[1]) == -1) {
			ft_putstr("error: cd: cannot change directory to ");
			ft_putstr(av[1]);
			ft_putstr("\n");
		}
	}
}

int    main(int argc, char **argv, char **envp)
{
	int	start = 0, end = start, cmd_in_fd = STDIN_FILENO;
	int	fd[2];

	(void) argc;
	argv = &argv[1];
	while (argv[end]) {
		if (strcmp(argv[end], "|") == 0) {
			argv[end] = NULL;
			pipe(fd); // Create pipe
			exec(argv + start, envp, cmd_in_fd, fd[1]); // Send pipe writ output
			close(fd[1]); // Close pipe output
			if (cmd_in_fd != STDIN_FILENO)
				close(cmd_in_fd);
			cmd_in_fd = fd[0]; // pipe input becomes current cmd input
			end++;
			start = end;
		} else if (strcmp(argv[end], ";") == 0) {
			argv[end] = NULL;
			exec(argv + start, envp, cmd_in_fd, STDOUT_FILENO); // Send out == 1
			while (waitpid(-1, NULL, 0) != -1) // Wait for prev cmd exec
				;
			if (cmd_in_fd != STDIN_FILENO)
				close(cmd_in_fd);
			cmd_in_fd = STDIN_FILENO; // current cmd input resets to STDIN
			end++;
			start = end;
		} else
			end++;
	}
	if (end > 0 && argv[end - 1] != NULL)
		exec(&argv[start], envp, cmd_in_fd, STDOUT_FILENO);
	if (cmd_in_fd != STDIN_FILENO)
		close(cmd_in_fd); 
	while (waitpid(-1, NULL, 0) != -1)
		;
	return 0;
}
