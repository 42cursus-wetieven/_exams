/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/23 10:05:30 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/25 23:30:17 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	inter(char *ref, char *cmp)
{
	int	i, j, k;

	i = 0;
	while (ref[i])
	{
		j = 0;
		while (cmp[j] && cmp[j] != ref[i])
			j++;
		if (cmp[j] == ref[i])
		{
			k = i;
			while (k-- && ref[k] != ref[i])
				 ;
			if (ref[k] != ref[i])
				write(1, &ref[i], 1);
		}
		i++;
	}
}

int		main(int ac, char **av)
{
	if (ac == 3)
	{
		write(1, av[1], 1);
		inter(++(av[1]), av[2]);
	}
	write(1, "\n", 1);
	return (1);
}
