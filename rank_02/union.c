/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/23 17:46:45 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/30 09:59:01 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	unionise(char *ref, char *cmp)
{
	int	i, j, k;

	i = 0;
	while (ref[i])
	{
		j = 0;
		while (cmp[j] && cmp[j] != ref[i])
			j++;
		if (cmp[j] != ref[i])
		{
			k = i;
			while (--k > 0 && ref[k] != ref[i])
				 ;
			if (ref[k] != ref[i] || !i)
				write(1, &ref[i], 1);
		}
		i++;
	}
}

void	inter(char *ref, char *cmp)
{
	int	i, j, k;

	i = 0;
	while (ref[i])
	{
		j = 0;
		while (cmp[j] && cmp[j] != ref[i])
			j++;
		if (cmp[j] == ref[i])
		{
			k = i;
			while (--k > 0 && ref[k] != ref[i])
				 ;
			if (ref[k] != ref[i] || !i)
				write(1, &ref[i], 1);
		}
		i++;
	}
}

int		main(int ac, char **av)
{
	if (ac == 3)
	{
		inter(av[1], av[1]);
		unionise(av[2], av[1]);
	}
	write(1, "\n", 1);
	return (1);
}
