/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/23 17:00:41 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/29 19:20:30 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

int	get_next_line(char **line)
{
	int			read_status;
	char		*bfr;
	size_t		capacity = 512;
	size_t		factor = 1;
	size_t		entries = 0;
	char		*old;
	size_t		i;

	if (read(0, 0, 0) || !line)
		return (-1);
	*line = NULL;
	if(!(bfr = malloc(capacity)))
		return (-1);
	do
	{
		if (entries == capacity)
		{
			old = bfr;
			factor++;
			capacity *= factor;
			if(!(bfr = malloc(capacity)))
				return (-1);
			i = 0;
			while (i < entries)
			{
				bfr[i] = old[i];
				i++;
			}
			free(old);
		}
		read_status = read(0, &bfr[entries], 1);
	} while (read_status > 0 && bfr[entries++] != '\n');
	if (read_status > 0)
		bfr[entries - 1] = '\0';
	*line = bfr;
	return (read_status);
}
