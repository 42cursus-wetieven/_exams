/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testgnl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/29 13:15:14 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/29 14:01:19 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "gnl.h"

int	main(void)
{
	char	*line;

	line = NULL;
	while (get_next_line(&line) > 0)
	{
		printf("%s\n", line);
		free(line);
	}
	return (0);
}
